import { render, screen } from "@testing-library/react"
import UserEvent from "@testing-library/user-event"
import Input from "."

describe("Input component", () => {
  it("should render the input component", () => {
    render(<Input />)
    expect(screen.getByRole("textbox")).toBeInTheDocument()
  })

  it("should change value with user input", () => {
    render(<Input />)
    UserEvent.type(screen.getByRole("textbox"), "Hello World")
    expect(screen.getByRole("textbox")).toHaveValue("Hello World")
  })
})
