import { useState } from "react"

const Input = (props) => {

  const [state, setState] = useState("")

  const handleChange = (event) => {
    setState(event.target.value)

    if (typeof props.onChange === "function") {
      props.onChange(event)
    }
  }

  return (
    <input
      name={props.name}
      onChange={handleChange}
      value={state}
      onBlur={props.onBlur}
    />
  )
}

export default Input
