import { fireEvent, render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import React from "react"
import { CustomEventComponent, Message } from "."

describe("CustomEventComponent", () => {
    it("should render string passed into greeting event", async () => {
        render(<Message />)

        const greeting = new CustomEvent("greeting", { detail: "Hello world" })

        // We can test that our subscribe is working here
        // by dispatching our custom event with fireEvent.

        fireEvent(document, greeting);

        expect(await screen.findByText("Hello world")).toBeInTheDocument()
    })
    it("should show greeting when i click the button", async () => {
        const user = userEvent.setup()

        render(<CustomEventComponent />)

        const button = screen.getByRole("button");

        user.click(button);

        expect(await screen.findByText("Hello!")).toBeInTheDocument()
    })
})