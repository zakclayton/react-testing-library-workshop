
import React, { useEffect, useState } from "react"
import { publish, subscribe, unsubscribe } from "./utils"

export const CustomEventComponent = () => {
    return (
        <>
            <Message />
            <Greeter />
        </>
    )
}

export const Message = () => {
    const [message, setMessage] = useState<string>()
    useEffect(() => {
        const setGreeting = (e: CustomEvent<string>) => setMessage(e.detail)
        subscribe("greeting", setGreeting)
        return () => {
            unsubscribe("greeting", setGreeting)
        }
    })

    return <h1>{message}</h1>
}

const Greeter = () => {
    return (
        <button onClick={() => {
            publish("greeting", "Hello!")
        }}>Greet</button>
    )
}