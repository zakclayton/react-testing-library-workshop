const subscribe = <T>(eventName: string, listener: (event: CustomEvent<T>) => void) => {
    document.addEventListener(eventName, listener as EventListener);
}
  
const unsubscribe = <T>(eventName: string, listener: (event: CustomEvent<T>) => void) => {
    document.removeEventListener(eventName, listener as EventListener);
}

const publish = <T>(eventName: string, data: T) => {
    const event = new CustomEvent<T>(eventName, { detail: data });
    document.dispatchEvent(event);
}
  
export { publish, subscribe, unsubscribe };