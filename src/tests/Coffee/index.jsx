import { useEffect, useState } from "react"

const Coffee = (props) => {
  const [file, setFile] = useState();

  useEffect(() => {
    async function getCoffee() {
      fetch("https://coffee.alexflipnote.dev/random.json")
        .then(res => {
          if (!res.ok) {
            throw new Error("Error")
          }
          return res.json()
        })
        .then(data => {
          if (data && data.file) {
            setFile(data.file)
          }
        })
    }

    getCoffee();
  }, [])

  return file ? (
    <img src={file} title="Coffee Please" alt="Random coffee" />
  ) : (
    <p>Awaiting...</p>
  )
}

export default Coffee
