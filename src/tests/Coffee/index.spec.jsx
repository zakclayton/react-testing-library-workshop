import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react"
import Coffee from "."

const mockFetch = () => {
  return jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => Promise.resolve({ file: "coffee.jpg" }),
    })
  )
}

describe("Coffee component", () => {
  it("should render loading text", () => {
    render(<Coffee />)
    expect(screen.getByText("Awaiting...")).toBeInTheDocument()
  })

  it("should fetch from API and display image", async () => {
    fetch = mockFetch(); // Jest runs in node and that doesn't support the fetch API

    render(<Coffee />)

    expect(screen.getByText("Awaiting...")).toBeInTheDocument()

    await waitFor(() => {
      expect(screen.getByAltText("Random coffee")).toBeInTheDocument()
    })

    expect(screen.queryByText("Awaiting...")).toBeNull()
  })

  // A different way of writing the same test.
  it("should fetch from API and remove loading text", async () => {
    fetch = mockFetch(); // Jest runs in node and that doesn't support the fetch API

    render(<Coffee />)

    expect(screen.getByText("Awaiting...")).toBeInTheDocument()

    await waitForElementToBeRemoved(screen.queryByText("Awaiting..."))

    expect(screen.getByAltText("Random coffee")).toBeInTheDocument()
  })
})
