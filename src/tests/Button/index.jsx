const Button = (props) => {
  return (
    <button aria-label="button-example" onClick={props.onClick}>{props.children}</button>
  )
}

export default Button
