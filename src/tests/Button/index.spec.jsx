import { screen, render, fireEvent } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import Button from "."

describe("Button component", () => {
  it("should assert with text", () => {
    render(<Button>Hello world</Button>)
    expect(screen.getByText("Hello world")).toBeInTheDocument()
  })
  it("should assert with label", () => {
    render(<Button>Hello world</Button>)
    expect(screen.getByLabelText(/button-example/)).toBeInTheDocument()
  })
  it("should assert text is not in the document", () => {
    render(<Button>Hello world</Button>)
    expect(screen.queryByText("Submit")).toBeNull()
  })
  it("should fire the on click event", () => {
    const fn = jest.fn()
    render(<Button onClick={fn}>Submit</Button>)

    fireEvent(screen.getByText("Submit"), new MouseEvent("click", {
      bubbles: true,
    }))

    expect(fn).toBeCalled()
  })
  it("should fire click even using userEvent", () => {
    const fn = jest.fn()

    render(<Button onClick={fn}>Submit</Button>)

    userEvent.click(screen.getByText("Submit"))

    expect(fn).toBeCalled()
  })
})
