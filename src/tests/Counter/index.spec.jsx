import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import Counter from "."

describe("Counter component", () => {
  it("should render the counter by default with 0 count", () => {
    render(<Counter />)
    expect(screen.getByText("The count is 0")).toBeInTheDocument()
  })

  it("should increment count on click", async () => {
    render(<Counter />)

    userEvent.click(screen.getByText("Increment"))

    expect(screen.queryByText("The count is 0")).toBeNull()
    expect(screen.getByText("The count is 1")).toBeInTheDocument()
  })
})
