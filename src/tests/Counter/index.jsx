import { useState } from "react"
import Button from "../Button"

const Counter = () => {
  const [n, setN] = useState(0);

  return (
    <>
      <p>The count is {n}</p>
      <Button onClick={() => setN(n + 1)}>Increment</Button>
    </>
  )
}

export default Counter
