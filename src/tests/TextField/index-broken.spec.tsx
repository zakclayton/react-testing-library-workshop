import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { WrappedTextField } from ".";

describe("WrappedTextField component", () => {
    describe("Fireevent", () => {
        it("should allow us to change the value", () => {
            render(<WrappedTextField />);
    
            const input: HTMLInputElement = screen.getByRole('textbox')
    
            fireEvent.change(input, { target: { value: "bar" }})
    
            expect(input).toHaveValue('bar')
        })
    })
    describe("Userevent", () => {
        it("should allow us to change the value", async () => {
            const user = userEvent.setup()

            render(<WrappedTextField />);
    
            const input: HTMLInputElement = screen.getByRole('textbox')
    
            await user.type(input, "bar")
    
            expect(input).toHaveValue('bar')
        })
    })
})