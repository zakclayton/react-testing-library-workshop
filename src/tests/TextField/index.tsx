import React, { useState } from "react"

export const TextField = () => {
    const [value, setValue] = useState<string>("")
    return (
        <input
            name="text-field"
            value={value}
            onChange={(e) => setValue(e.target.value)}
        />
    )
}

export const WrappedTextField = ({ label }: { label?: string }) => {
    return (
        <label
            onKeyDown={(e) => {
                // Pretend we do something clever here.
                e.preventDefault()
            }}
        >
            {label}
            <TextField />
        </label>
    )
}