import React from "react";
import { fireEvent, render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event";
import { TextField } from ".";

describe("TextField component", () => {
    describe("Fireevent example", () => {
        it("should allow us to change the value", () => {
            render(<TextField />);
    
            const input: HTMLInputElement = screen.getByRole('textbox')
    
            fireEvent.change(input, { target: { value: "bar" }})
    
            expect(input).toHaveValue('bar')
        })
    })
    describe("Userevent example with user setup (recommended)", () => {
        it("should allow us to change the value", async () => {
            const user = userEvent.setup()

            render(<TextField />);
    
            const input: HTMLInputElement = screen.getByRole('textbox')
    
            await user.type(input, "bar")
    
            expect(input).toHaveValue('bar')
        })
    })
    describe("Userevent example direct api", () => {
        it("should allow us to change the value", async () => {
            render(<TextField />);
    
            const input: HTMLInputElement = screen.getByRole('textbox')
    
            await userEvent.type(input, "bar")
    
            expect(input).toHaveValue('bar')
        })
    })
})