import './App.css';
import React from 'react';
import { CustomEventComponent } from './tests/CustomEvent';
import { TextField, WrappedTextField } from './tests/TextField';

function App() {
  return (
    <div className="App">
      <div>
        <h1>Text Field Example</h1>
        <label>
          Text field:{' '}
          <TextField />
        </label>
        <br />
        <br />
        <WrappedTextField label="Text field: " />
        <br />
        <br />
      </div>
      <hr />
      <div>
        <h1>Greeting Example</h1>
        <CustomEventComponent />
      </div>
    </div>
  );
}

export default App;
